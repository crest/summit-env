# Summit Env

This repo acts as a central location to deploy artifacts into the various 
environments (QA, pprd, prod) using AWS ECS.

## Motivation

Before having this single repository, we ran into lots of problems. Some of them are...

- Each component would deploy itself into the cluster, but also be responsible for hooking it to the appropriate dependencies. This was difficult for the frontend, as it needs to be configured with a backend to talk to
- In order for the frontend to be configured correctly, the backend needed to exist first. This forced timing caused issues and required redeploys to fix configuration.
- For changes that were backend only, we still needed a connected frontend to perform acceptance testing. To do so, we created branches solely to have a deploy. But, these were frequently overlooked and left as orphaned deploys.
- Having multiple frontends pointing to the same backend (most frequently a master API) caused CORS-related issues. When using the frontend for Branch X, the browser would cache the successful CORS response. But, when swapping to Branch Y, the browser would remember the cached response for Branch X (which is not Branch Y) and fail the request.
- Occasionally, testers would be confused seeing data in Branch X also appear in Branch Y, as they were sharing the same backend.

To overcome all of these issues, we determined it would be best to create an isolated stack for every feature branch. It removes any complex service discovery needs, timing issues, and ensures data isolation between branches. By pushing a new feature branch to any component branch, a stack will be created for that feature branch.


## `summit-env` branches

There are two types of branches within this repo:

- **Build branch** - this branch (`update-stack`) is configured to create/update/delete feature branches, as documented below
- **Feature branches** - this consists of all other branches. Each branch represents a single deployment and contains a `docker-stack.yml` file that is specifically configured for that deployment. As updates occur in upstream components, the stack file will be updated to reflect the updates for the modified component.


## Build/Deploy Process

Summit is composed of many components (API, desktop client, docs, etc.). Each component exists in its own repo. But, in order to simplify testing/deploying, they are deployed as a "single" unit or stack. Since there are multiple repos, this brings multiple build pipelines. The responsibilities of both component and env repos are outlined below.

### Responsibilities of each Component

- Each component is responsible for building and testing itself using its own pipeline. The output of the build is a Docker image that is capable of running the component.
- After building the Docker image, each component pipeline must trigger a build on this repo's `update-stack` branch passing along the properties documented below.

Tips:

- To better ensure deployments are successful, Docker images produced by the component should be tagged in such a way as to prevent tag collisions. As an example, the image for the API could reflect the pattern of `summit/denali:COMMIT-JOB`, where COMMIT is the commit hash and JOB is the job number for the build. This ensures that a subsequent build on the same commit causes a new tag to be created, but still provides traceability as to what was built.

### Responsibilities of the `summit-env/update-stack` branch

The `update-stack` branch is designed to _only_ create/update/delete other branches in the summit-env repo. It is intended to be built only via remote triggers and relies on build parameters, as documented below. During the build, it will update the stack file as appropriate and push the change back to the repo.

#### Build Parameters

The following parameters must be provided when running a build on the `update-stack` branch.

- `ENV_BRANCH` - environment branch name (the branch that will be updated/created)
- `UPDATED_SERVICE` - the name of the service being updated. The name should reflect the name of the service found in the stack file
- `NEW_IMAGE` - new image value (full path, including tag)
- `ACTION` - the action being performed. Current values only support `deploy` and `undeploy`

#### Update Process

During the update, several things occur, which are displayed in the flowchart below. A few notable items:

- Each service has a "source" (indicated by the `source-branch` label). When a feature branch is first created, all services are sourced from master. As deploys occur to services, the label is updated to the branch. When a service is undeployed, it's source is reverted back to master (even if it's not really master). Once all sources are master again, the feature branch is no longer needed and deleted.

![Update Stack flowchart](/images/update-stack-flowchart.png)


### Responsibilities of the `summit-env` feature branches

Each feature branch is simply used to deploy the stack to QA and beyond (if on master/stable).

- Every push to the feature branch will deploy the updated stack to the QA environment. 
- When the branch is deleted, the `undeploy-qa` job runs, removing the stack from the QA environment.


## FAQs

### I need to change the image for a deployed service. Where do I do that?

If the image is already built and you just need to deploy it (for example, a rollback), simply check out the branch for the stack and update the stack file. When you push the updated stack file, it will deploy the stack, telling Swarm to use the new image. In this case, _you_ are acting as the update agent, instead of the `update-stack` build.
