ARG IMAGE

FROM $IMAGE
ARG TAG
ARG ADMIN_TAG
ARG AGREEMENTS_TAG
ARG API_TAG
ARG DOCS_TAG
ARG EMAIL_TEMPLATES_TAG
ARG HELP_TAG
ARG MOBILE_TAG
ARG PROPOSAL_TAG
RUN echo "{\"version\":\"$TAG\",\"components\":{\"admin\":\"$ADMIN_TAG\",\"agreements\":\"$AGREEMENTS_TAG\",\"api\":\"$API_TAG\",\"docs\":\"$DOCS_TAG\",\"help\":\"$HELP_TAG\",\"proposals\":\"$PROPOSAL_TAG\",\"mobile\":\"$MOBILE_TAG\",\"email-templates\":\"$EMAIL_TEMPLATES_TAG\"}}" > /manifest.json

