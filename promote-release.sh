#!/bin/sh

DEPLOYMENT_TIERS="pprd prod"
DOMAIN_NAME="summit.cloud.vt.edu"
ERRORS_URL="https://errors.summit.cloud.vt.edu/v1"
FRONT_END_MANIFEST="front-end-manifest.json"
FRONT_END_ASSETS_BUCKET="summit-front-end-assets"
RELEASE_TAG_PATTERN="^[0-9]+\.[0-9]+\.[0-9]+(-SNAPSHOT(\.[0-9]+)?)?$"

QA_NAME_NOOP=qa/noop
RELEASE_NAME_NOOP=releases/noop
IMAGE_TAG_NOOP=52a7b91dac2cb9efcd3c752d1b08eaaccae669dd


function create_front_end_manifest() {
  local release_tag=$1
  cat manifest.yml \
      | yq to_json \
      | jq 'to_entries | .[] | select(.value.release_target == "frontend") | {(.value.component_name): .value.image.tag}' \
      | jq -s add \
      | jq "{\"version\": \"$release_tag\", \"components\": .}" \
      > $FRONT_END_MANIFEST

  echo "Component manifest for the release"
  cat $FRONT_END_MANIFEST
}


function promote_noop_image() {
  local src=${AWS_REGISTRY_HOST}/${QA_NAME_NOOP}:${IMAGE_TAG_NOOP}
  local dest=${AWS_REGISTRY_HOST}/${RELEASE_NAME_NOOP}:latest
  echo "Promoting $src"
  echo "       as $dest"
  docker image pull $src
  docker image tag $src $dest
  docker image push $dest
}


function promote_front_end_images() {
  local release_tag=$1
  local docker_file=$(mktemp ./DockerfileXXXXXXXX)
  local image_data=$(mktemp)

  cat <<EOF >$docker_file
ARG SOURCE_IMAGE
FROM \${SOURCE_IMAGE}
COPY $FRONT_END_MANIFEST /manifest.json
EOF

  cat manifest.yml \
      | yq to_json \
      | jq "to_entries | .[] | select(.value.release_target==\"frontend\") | {(.value.image.name + \":\" + .value.image.tag): (\"releases/\" + .value.component_name + \":$release_tag\")}" \
      | jq -s add \
      | jq -r 'to_entries | .[] | .key + " " + .value' \
      > $image_data

  (
    while read image_detail; do
      src="${AWS_REGISTRY_HOST}/$(echo $image_detail | cut -f1 -d' ')"
      dest="${AWS_REGISTRY_HOST}/$(echo $image_detail | cut -f2 -d' ')"
      echo
      echo
      echo "Promoting $src"
      echo "       as $dest"
      docker image build --file $docker_file --pull --build-arg SOURCE_IMAGE=$src --tag $dest .
      docker image push $dest
    done
  ) < $image_data

  rm $docker_file
}


function promote_back_end_images() {
  local release_tag=$1
  local image_data=$(mktemp)

  cat manifest.yml \
    | yq to_json \
    | jq "to_entries | .[] | select(.value.release_target==\"backend\") | {(.value.image.name + \":\" + .value.image.tag): (\"releases/\" + .value.component_name + \":$release_tag\")}" \
    | jq -s add \
    | jq -r 'to_entries | .[] | .key + " " + .value' \
    > $image_data

  (
    while read image_detail; do
      src="${AWS_REGISTRY_HOST}/$(echo $image_detail | cut -f1 -d' ' | sed -E 's/-qa:/:/')"
      dest="${AWS_REGISTRY_HOST}/$(echo $image_detail | cut -f2 -d' ')"
      echo
      echo
      echo "Promoting $src"
      echo "       as $dest"
      docker image pull $src
      docker image tag $src $dest
      docker image push $dest
    done
  ) < $image_data

}


function release_front_end_assets() {
  local release_tag=$1
  local image_data=$(mktemp)
  local env_file=$(mktemp ./envXXXXXXXX)

  cat manifest.yml \
      | yq to_json \
      | jq "to_entries | .[] | select(.value.release_target==\"frontend\") | {(.value.component_name): (\"releases/\" + .value.component_name + \":$release_tag\")}" \
      | jq -s add \
      | jq -r 'to_entries | .[] | .key + " " + .value' \
      > $image_data

  (
    while read image_detail; do
      component_name=$(echo $image_detail | cut -f1 -d' ')
      image_name="${AWS_REGISTRY_HOST}/$(echo $image_detail | cut -f2 -d' ')"

      cat <<EOF >$env_file
COMPONENT=${component_name}
DEPLOYMENT_TIERS=${DEPLOYMENT_TIERS}
DOMAIN_NAME=${DOMAIN_NAME}
OAUTH_ENABLED=true
ERRORS_URL=${ERRORS_URL}
S3_BUCKET=${FRONT_END_ASSETS_BUCKET}
S3_PREFIX=${release_tag}/${component_name}
EOF

      echo
      echo
      echo "Releasing $component_name assets in image $image_name"
      docker container run --rm --env AWS_ACCESS_KEY_ID --env AWS_SECRET_ACCESS_KEY --env-file ${env_file} \
          ${image_name} /app/release.sh

    done
  ) < $image_data
}


if [ -z "$AWS_REGISTRY_HOST" ]; then
  echo "AWS_REGISTRY_HOST environment variable is required" >&2
  exit 1
fi

release_tag=$1
if [ -z "$release_tag" ]; then
	echo "Usage: $(basename $0) <release-tag>" >&2
	exit 1
fi

echo $release_tag | grep -E $RELEASE_TAG_PATTERN >/dev/null
if [ $? -ne 0 ]; then
  echo "Release tag must be of the form X.Y.Z where X, Y, Z are integral numbers" >&2
  exit 1
fi

set -e
create_front_end_manifest $release_tag
promote_noop_image
promote_front_end_images $release_tag
promote_back_end_images $release_tag
release_front_end_assets $release_tag
