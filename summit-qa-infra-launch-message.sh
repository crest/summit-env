#!/bin/sh

stack_id=$1
if [ -z "$stack_id" ]; then
	echo "usage: $(basename $0) <stack-id>" >&2
	exit 1
fi

# need yq to translate YAML to JSON
# version must match what is needed for awscli
pip install --upgrade pip yq==2.12.2 >/dev/null  

message_file=$(mktemp)

cat manifest.yml \
    | yq \
    | jq 'to_entries | .[] | select(.value.component_name != "none") | {(.value.component_name): .value.image.tag}' \
    | jq -s add \
    | jq "{\"eventType\": \"Launch\", \"stackId\": \"$stack_id\", \"Launch\": { \"pipelineId\": \"$CI_PIPELINE_ID\", \"jobId\": \"$CI_JOB_ID\", \"manifest\": . }}" \
    > ${message_file}

# show the nicely formatted message in console output
cat ${message_file} >&2             

# remove indentation and newlines for return value
jq -r tostring < ${message_file}     
