#!/bin/sh
#
# Produces a configuration file for summit-cli
#

# Errors are fatal
set -e

# Produce a list of all of the components that get a CLI profile
cat manifest.yml | \
    yq -o json eval 'to_entries | .[] | select(.value.cli) | {.value.cli.profile: .value.cli.description}' | \
    jq -sr 'add | keys_unsorted | .[]' > profiles.txt

# Tack on additional special-case profiles
echo "ft" >> profiles.txt
echo "metrics" >> profiles.txt

# Turn the list of profiles into a shell variable assignment to PROFILES
(
  profiles=""
  while read profile
  do
    profiles="$profiles $profile"
  done
  echo "PROFILES=\"${profiles}\"" | sed 's/=" */="/' >> /app/cli.conf
) < profiles.txt

rm profiles.txt

# Get the list of the descriptions for each CLI profile
cat manifest.yml \
  | yq -o json eval 'to_entries | .[] | select(.value.cli) | {.value.cli.profile: .value.cli.description}' \
  | jq -sr 'add | values | .[]' > descriptions.txt

# Produce a sequence of shell variable assignments of the form DESCRIPTIONS_n="some description"
(
  i=1
  while read description
  do
    echo "DESCRIPTIONS_${i}=\"${description}\"" >>/app/cli.conf
    i=$(expr $i + 1)
  done
  # Tack on additional special-case profile descriptions
  echo "DESCRIPTIONS_${i}=\"Selenium Grid\"" >>/app/cli.conf
  i=$(expr $i + 1)
  echo "DESCRIPTIONS_${i}=\"Grafana/Prometheus\"" >>/app/cli.conf
) < descriptions.txt

rm descriptions.txt
