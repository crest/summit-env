#!/bin/sh
mkdir -p build/summit-in-a-box
cp manifest.yml registry.json build/summit-in-a-box
tar -C summit-in-a-box -cf - . | tar -C build/summit-in-a-box -xf -
echo AWS_REGISTRY_HOST=${AWS_REGISTRY_HOST} >> build/summit-in-a-box/environment
