terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.10.0"
    }
  }
  backend "s3" {
    bucket = "summit-terraform-state"
    key    = "summit-in-a-box/base.json"
  }
}

locals {
  product_name = "summit-in-a-box"
  component_name = "in-a-box"
}

provider "aws" {
  default_tags {
    tags = {
      Owner = local.product_name
    }
  }
}

module qa_ecr_repository {
  source = "git::ssh://git@code.vt.edu/crest/summit-terraform.git//qa_ecr_repository"
  component_name = local.component_name
  image_tag_mutability = true
}

module ci_pipeline_user {
  source = "git::ssh://git@code.vt.edu/crest/summit-terraform.git//ci_pipeline_user_ecr_only"
  product_name = local.product_name
  ecr_repository_arn = module.qa_ecr_repository.arn
}
